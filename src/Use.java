import book.BookHashSet;
import record.RecordList;
import user.Admin;
import user.Everyman;
import user.User;

import java.util.Scanner;

public class Use {
    public static User login(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入你的姓名");
        String name = scanner.nextLine();
        System.out.println("请输入你的身份：1-->管理员 0-->普通人");
        int choice = scanner.nextInt();
        if (choice == 1){
            return new Admin(name);
        }else {
            return new Everyman(name);
        }
    }

    public static void main(String[] args) {
        BookHashSet bookHashSet = new BookHashSet();
        RecordList recordList=new RecordList();
        User user = login();
        while (true){
            int choice = user.menu();//多态确定身份
            user.doOperation(choice,bookHashSet,recordList);
        }
    }
}