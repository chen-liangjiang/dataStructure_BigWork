package book;

public class Book {
    private  int id;
    private String name;
    private String author;
    private Integer price;
    private  String type;
    private boolean status;//false为还在，ture已被借走
    private  Integer num;

    private String ISBN; //标识图书的国际标准编号

    public Book(int id, String name, String author, Integer price, String type, boolean status, Integer num,String ISBN) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.price = price;
        this.type = type;
        this.status = status;
        this.num = num;
        this.ISBN = ISBN;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean isStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
    public Boolean getStatus(boolean status) {
        return status;
    }


    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", price=" + price +
                ", type='" + type + '\'' +
//                ", status=" + status +
                ( (status) ? " 借出 " : " 未借出 ")+","
                + "ISBN='"+ISBN+'\''+","+
                "还剩："+num+"'件'"
                + '}'
                ;
    }

}
