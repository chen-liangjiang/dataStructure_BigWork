package book;

import java.util.HashSet;

/**
 * @Author Mr.chen
 * @Description TODO
 * @Date 2023/6/24 0:14
 * @Version 1.0
 */
public class BookHashSet extends CustomHashSet{
    public BookHashSet() {
        super();
        Book[] books = new Book[10];
        // 创建图书对象数组并添加元素
        books[0] = new Book(1,"三国", "罗贯中", 15, "小说",false,12,"10001212");
        books[1] = new Book(2,"西游记", "吴承恩", 16, "小说",false,12,"1234567");
        books[2] = new Book(3,"水浒传", "施耐庵", 17, "小说",false,13,"243144314");
        books[3] = new Book(4,"红楼梦", "曹雪芹", 18, "小说",false,13,"354145151");
        books[4] = new Book(5,"Java基础", "黑马程序员", 19, "小说",false,13,"123451");
        for (int i = 0; i < books.length && books[i] != null; i++) {
            add(books[i]);
        }

    }

}
