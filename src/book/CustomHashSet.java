package book;

/**
 * 自定义的哈希集合类
 */
public class CustomHashSet {
    private static final int DEFAULT_CAPACITY = 16; // 默认初始容量
    private static final float LOAD_FACTOR = 0.75f; // 负载因子
    private Node[] table; // 哈希表数组
    private int size; // 集合中元素的数量

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    /**
     * 节点类
     */
    private static class Node {
        private Book book;
        private Node next;

        public Node(Book book) {
            this.book = book;
        }
    }

    public CustomHashSet() {
        table = new Node[DEFAULT_CAPACITY];
        size = 0;
    }

    /**
     * 计算哈希值
     *
     * @param key 键值
     * @return 哈希值
     */
    private int hash(int key) {
        return key % table.length;
    }

    /**
     * 添加元素
     *
     * @param book 要添加的图书对象
     */
    public boolean add(Book book) {
        int index = hash(book.getId());
        Node newNode = new Node(book);
        if (table[index] == null) {
            //若位置为空则插入
            table[index] = newNode;
        } else {
            Node curr = table[index];
            while (curr.next != null) {
                if (curr.book.equals(book)) {
                    return false; // 图书已存在，添加失败
                }
                curr = curr.next;
            }
            curr.next = newNode;
        }
        size++;
        return true; // 成功添加图书
    }

    /**
     * 删除元素
     *
     * @param book 要删除的图书对象
     */
    public void remove(Book book) {
        int index = hash(book.getId());
        if (table[index] == null) {
            return;
        }
        if (table[index].book.equals(book)) {
            table[index] = table[index].next;
            size--;
            return;
        }
        Node curr = table[index];
        Node prev = null;
        while (curr != null && !curr.book.equals(book)) {
            prev = curr;
            curr = curr.next;
        }
        if (curr != null) {
            prev.next = curr.next;
            size--;
        }
    }

    /**
     * 根据图书名删除图书
     *
     * @param bookName 要删除的图书名
     */
    public void removeByBookName(String bookName) {
        for (int i = 0; i < table.length; i++) {
            if (table[i] == null) {
                continue;
            }
            if (table[i].book.getName().equals(bookName)) {
                table[i] = table[i].next;
                size--;
                return;
            }
            Node curr = table[i];
            Node prev = null;
            while (curr != null && !curr.book.getName().equals(bookName)) {
                prev = curr;
                curr = curr.next;
            }
            if (curr != null) {
                prev.next = curr.next;
                size--;
                return;
            }
        }
    }

    /**
     * 根据图书ISBN删除图书
     *
     * @param isbn 要删除的图书ISBN
     */
    public void removeByISBN(String isbn) {
        for (int i = 0; i < table.length; i++) {
            if (table[i] == null) {
                continue;
            }
            if (table[i].book.getISBN().equals(isbn)) {
                table[i] = table[i].next;
                size--;
                return;
            }
            Node curr = table[i];
            Node prev = null;
            while (curr != null && !curr.book.getISBN().equals(isbn)) {
                prev = curr;
                curr = curr.next;
            }
            if (curr != null) {
                prev.next = curr.next;
                size--;
                return;
            }
        }
    }

    /**
     * 显示所有图书
     */
    public void displayBooks() {
        System.out.println("图书列表：");
        for (int i = 0; i < table.length; i++) {
            Node curr = table[i];
            while (curr != null) {
                System.out.println(curr.book);
                curr = curr.next;
            }
        }
    }

    /**
     * 检查集合中是否包含指定的图书对象
     *
     * @param book 要检查的图书对象
     * @return true，如果包含；false，如果不包含
     */
    public boolean containsBook(Book book) {
        for (int i = 0; i < table.length; i++) {
            Node curr = table[i];
            while (curr != null) {
                if (curr.book.equals(book)) {
                    return true;
                }
                curr = curr.next;
            }
        }
        return false;
    }

    /**
     * 返回集合中的图书数量
     *
     * @return 图书数量
     */
    public int getBookCount() {
        return size;
    }

    /**
     * 更新图书信息
     *
     * @param bookId      图书ID
     * @param updatedBook 更新后的图书对象
     */
    public void updateBook(int bookId, Book updatedBook) {
        for (int i = 0; i < table.length; i++) {
            Node curr = table[i];
            while (curr != null) {
                if (curr.book.getId() == bookId) {
                    curr.book.setName(updatedBook.getName());
                    curr.book.setAuthor(updatedBook.getAuthor());
                    curr.book.setPrice(updatedBook.getPrice());
                    curr.book.setType(updatedBook.getType());
                    curr.book.setStatus(updatedBook.isStatus());
                    curr.book.setNum(updatedBook.getNum());
                    curr.book.setISBN(updatedBook.getISBN());
                    System.out.println("成功更新图书信息：" + curr.book);
                    return;
                }
                curr = curr.next;
            }
        }
        System.out.println("找不到指定ID的图书");
    }


    public Book get(int index) throws IndexOutOfBoundsException {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index out of range: " + index);
        }
        int count = 0;
        for (int i = 0; i < table.length; i++) {
            Node curr = table[i];
            while (curr != null) {
                if (count == index) {
                    return curr.book;
                }
                count++;
                curr = curr.next;
            }
        }
        return null;
    }

    /**
     * 根据下标替换元素
     *
     * @param index 下标
     * @param element 要替换的元素
     * @return 旧的元素
     * @throws IndexOutOfBoundsException 如果下标越界，则抛出异常
     */
    public Book set(int index, Book element) throws IndexOutOfBoundsException {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index out of range: " + index);
        }
        int count = 0;
        Node prev = null;
        Node curr = null;
        for (int i = 0; i < table.length; i++) {
            curr = table[i];
            while (curr != null) {
                if (count == index) {
                    break;
                }
                count++;
                prev = curr;
                curr = curr.next;
            }
            if (curr != null) {
                Book oldValue = curr.book;
                curr.book = element;
                return oldValue;
            }
        }
        return null;
    }
}