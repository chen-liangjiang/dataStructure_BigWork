package user;

import operation.*;

import java.util.Scanner;

public class Admin extends User {
    public Admin(String name) {
        super(name);
        this.operations = new Operation[]{
                new ExitOperation(),
                new FindOperation(),
                new AddOperation(),
                //new NewAddOperation(), // 删除了这个，使用上面一个
                new NewDeleteOperation(),
               // new DisplayOperation(),//删除该类，使用newdisplayoperation
                new NewDisplayOperation(),
                new ModOperation()
        };
    }

    @Override
    public int menu() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("管理员菜单");
        System.out.println("=======================");
        System.out.println("hello" +"\t"+ this.name+"欢迎来到图书系统");
        System.out.println("1.查找图书");
        System.out.println("2.新增图书");
        System.out.println("3.删除图书");
        System.out.println("4.显示图书");
        System.out.println("5.修改书籍");
        System.out.println("0.退出系统");
        int choice = scanner.nextInt();
        return choice;
    }
}