package user;

import operation.*;

import java.util.Scanner;

public class Everyman extends User {

    public Everyman(String name) {
        super(name);
        this.operations =new Operation[]{
                new ExitOperation(),
                new FindOperation(),
               // new BorrowOperation(),
                new NewBorrowOperation(),
                new ReturnOperation()
        };
    }

    @Override
    public int  menu() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("普通用户菜单");
        System.out.println("=======================");
        System.out.println("hello" + this.name+"欢迎来到图书系统");
        System.out.println("1.查找图书");
        System.out.println("2.借阅图书");
        System.out.println("3.归还图书");
        System.out.println("0.退出系统");
        int choice = scanner.nextInt();
        return choice;
    }

}