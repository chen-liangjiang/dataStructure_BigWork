package user;

import book.BookHashSet;
import operation.Operation;
import record.RecordList;

import java.io.IOException;

public abstract class User {
    public String name;
    public Operation[] operations;

    public User(String name) {
        this.name = name;
    }

    public abstract int  menu();

    public void doOperation(int choice, BookHashSet bookList, RecordList recordList){
        try {
            this.operations[choice].work(bookList,recordList);//对应操作集合里的操作，每个操作都有一个work方法
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}