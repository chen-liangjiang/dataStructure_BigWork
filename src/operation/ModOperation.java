package operation;
import book.Book;
import book.BookHashSet;
import record.RecordList;

import java.io.IOException;
import java.util.Scanner;
//修改图书
public class ModOperation implements Operation {

    @Override
    public void work(BookHashSet bookList, RecordList recordList) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入要修改的书籍的编号：");
        int id = scanner.nextInt();
        for (int a = 0; a <bookList.getBookCount() ; a++) {
            Book book = bookList.get(a);
            if (book.getId() == id) {
                System.out.println("请输入要修改的书籍的名称");
                String name = scanner.next();
                System.out.println("请输入要修改的书籍的作者");
                String author = scanner.next();
                System.out.println("请输入要修改的书籍的价格");
                int price = scanner.nextInt();
                System.out.println("请输入要修改的书籍的种类");
                String type = scanner.next();
                System.out.println("请输入要修改的书籍的数量");
                int num = scanner.nextInt();
                System.out.println("请输入要修改的书籍的ISBN");
                String isbn = scanner.next();
                System.out.println("请输入要修改的书籍的状态,0代表还有剩，1代表已经没有了");
                int  i = scanner.nextInt();
                boolean status;
                if(i ==0){
                    status = false;
                }else {
                    status=true;
                }
                Book book1 = new Book(id,name,author,price,type,status,num,isbn);
                bookList.updateBook(id,book1);
                System.out.println("修改书籍完毕！");
                return;
            }
        }
        System.out.println("你要修改的书籍不存在");

        }


    }