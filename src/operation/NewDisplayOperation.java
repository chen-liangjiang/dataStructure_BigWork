package operation;

import book.BookHashSet;
import record.RecordList;

/**
 * @Description: ${description}  // 类说明，在创建类时要填写
 * @ClassName: NewDisplayOperation    // 类名，会自动填充
 * @Author: weisn          // 创建者
 * @Date: 2023/6/23 12:03   // 时间
 * @Version: 1.0     // 版本
 */
public class NewDisplayOperation implements Operation{
    @Override
    public void work(BookHashSet bookList, RecordList recordList) {
        bookList.displayBooks();
    }


}
