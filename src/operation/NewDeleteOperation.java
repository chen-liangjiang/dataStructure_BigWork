package operation;

import book.Book;
import book.BookHashSet;
import record.Record;
import record.RecordList;

import java.util.Date;
import java.util.Scanner;

/**
 * @Description: 使用hash表进行删除  // 类说明，在创建类时要填写
 * @ClassName: NewDeleteOperation    // 类名，会自动填充
 * @Author: qiu       // 创建者
 * @Date: 2023/6/23 11:29   // 时间
 * @Version: 1.0     // 版本
 */
public class NewDeleteOperation  implements Operation {

    @Override
    public void work(BookHashSet bookSet, RecordList recordList) {
        System.out.println("删除书籍");
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入按照什么删除，0是按照书名删除，1是按照ISBN删除，请输入");
        int i = scanner.nextInt();
        if (i == 0) {
            System.out.println("请输入要删除的书名");
            String name = scanner.next();
            for (int a = 0; a <bookSet.getBookCount() ; a++) {
                Book book = bookSet.get(a);
                if (book.getName().equals(name)) {
                    bookSet.removeByBookName(name);
                    System.out.println("删除书籍完毕！");
                   return;
                }
            }
            System.out.println("没有该书籍");
        } else {
            System.out.println("请输入要删除的书的ISBN");//检验判断是否存在
            String isbn = scanner.next();
            for (int a = 0; a <bookSet.getBookCount() ; a++) {
                Book book = bookSet.get(a);
                if (book.getISBN().equals(isbn)) {
                    bookSet.removeByISBN(isbn);
                    System.out.println("删除书籍完毕！");
                    return;
                }
            }
        }


    }
}
