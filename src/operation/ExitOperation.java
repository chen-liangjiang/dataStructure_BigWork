package operation;

import book.BookHashSet;
import record.RecordList;

public class ExitOperation implements Operation {
    @Override
    public void work(BookHashSet bookList, RecordList recordList) {
        System.out.println("退出系统");
        System.exit(1);
    }


}