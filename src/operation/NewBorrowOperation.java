package operation;

import book.Book;
import book.BookHashSet;
import record.Record;
import record.RecordList;

import java.util.Date;
import java.util.Scanner;

public class NewBorrowOperation implements Operation{
    @Override
    public void work(BookHashSet bookList, RecordList recordList) {
        System.out.println("借阅书籍");
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入要借阅的书名");
        String name = scanner.nextLine();

        for (int i = 0; i <bookList.getBookCount() ; i++) {
            Book book = bookList.get(i);
            if (book.getName().equals(name)){
                //说明有这本书！
                //查看状态
                if (book.isStatus())
                {
                    System.out.println("该书已无库存");
                    return;
                }
                //没被借走则把这本书的借阅状态修改为true
                Record record=new Record(recordList.usedSize,"张三",i,name, new Date(),null,false);
                //加入借阅书籍时增加数据
                recordList.add(record);
                //设置状态
                book.setStatus(true);
                System.out.println( "恭喜借到"+ book);
                return;
            }
        }
        System.out.println("没有要找的书籍！！");
    }
}
