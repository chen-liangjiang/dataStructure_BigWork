package operation;

import book.Book;
import book.BookHashSet;
import record.RecordList;

import java.util.Scanner;

public class AddOperation implements Operation {

    @Override
    public void work(BookHashSet bookList, RecordList recordList) {
        System.out.println("增加书籍 ");
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入图书的名字");
        String name = scanner.nextLine();
        System.out.println("请输入图书的作者");
        String author = scanner.nextLine();
        System.out.println("请输入图书的价格");
        int price = scanner.nextInt();
        System.out.println("请输入图书的类型");
        String type = scanner.next();
        System.out.println("请输入图书的数量");
        int num = scanner.nextInt();
        System.out.println("请输入图书的国际唯一标识");
        String ISBM = scanner.next();
        //修改id
        int id = bookList.getBookCount()+1;
        Book book = new Book(id,name,author,price,type,false,num,ISBM);
        //方便确定位置，不用一次次遍历
        int currentSize = bookList.getBookCount();

        bookList.add(book);

    }


}