package operation;

import book.Book;
import book.BookHashSet;
import record.RecordList;
import java.util.Scanner;

public class ReturnOperation implements Operation {
    @Override
    public void work(BookHashSet bookList, RecordList recordList){
        System.out.println("归还书籍");
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入要归还的书名");
        String name = scanner.nextLine();
        for (int i = 0; i < bookList.getBookCount(); i++) {
            Book book = bookList.get(i);
            if(book.getName().equals(name)) {
                book.setStatus(false);
                System.out.println(book);
                return;
            }
        }
        System.out.println("没有你要归还的这本书！");
    }
}