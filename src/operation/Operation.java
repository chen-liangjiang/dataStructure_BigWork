package operation;
import book.BookHashSet;
import record.RecordList;

import java.io.IOException;

public interface Operation {
    void  work(BookHashSet bookList, RecordList recordList) throws IOException;
}