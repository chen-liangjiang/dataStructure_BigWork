package record;

import book.Book;

import java.time.LocalDate;
import java.util.Date;

public class RecordList {
    public int usedSize;
    private Record[] records;

    public RecordList() {
        this.records = new Record[10];
        records[0]=new Record(1,"张三",1,"三国", new Date(),null,false);
        records[1]=new Record(2,"张三",2,"西游记", new Date(),null,false);
        records[2]=new Record(3,"李四",3,"水浒传", new Date(),null,false);
        this.usedSize=3;
    }

    public Record[] getRecords() {
        return records;
    }

    public void setRecords(Record[] records) {
        this.records = records;
    }

    public int getUsedSize() {
        return usedSize;
    }

    public void setUsedSize(int usedSize) {
        this.usedSize = usedSize;
    }

    public void add(Record record) {
        int a = this.getUsedSize();
        records[a] = record;
        this.usedSize++;
    }
}
